import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableWithoutFeedback, Video, Button, TextInput,TouchableHighlight } from 'react-native';
import {useDimensions, useDeviceOrientation } from '@react-native-community/hooks';
export default function App() {
  console.log(useDimensions());
  console.log(useDeviceOrientation());
  return (
    <Video source={{ uri: 'http://STREAM_URL/playlist.m3u8' }} />
    <View style={styles.container}>
     
      <TouchableWithoutFeedback onPress={ ()=> console.log("image tapped")}>
      <Image source={ require('./assets/keyoss.png')} style={{ height:250,width:250}}/>

      </TouchableWithoutFeedback>

      <Text style={{color:'white',fontSize:36}}>Votre Nom : </Text>       
      <TextInput
        style={{
          height: 40,
          width:200,
          borderColor: 'gray',
          borderWidth: 1,
          backgroundColor:'white'
        }}
      />
      <Text style={{color:'white',fontSize:36}}>Mot de Passe :</Text> 
      <TextInput
        style={{
          height: 40,
          width:200,
          borderColor: 'gray',
          borderWidth: 1,
          backgroundColor:'white',
          marginBottom:30
        }}
      />
      <StatusBar style="auto" />

    
        <TouchableHighlight   
        activeOpacity={0.6}
        underlayColor="#DDDDDD"
        onPress={() => alert('Logged in!')}>

          <Text style={{
                backgroundColor:'mediumspringgreen',
                padding:15,
               
                color:'white',
                fontSize:30,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 20,
          }}>Login</Text>
        
        </TouchableHighlight>
   

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(43,42,42)',
    alignItems: 'center',
    justifyContent: 'center',
    color:'white'
  },
});
